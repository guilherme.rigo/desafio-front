import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {AppComponent} from './app.component';
import {SimulacaoComponent} from './simulacao/simulacao.component';
import {ViewSimulacoesComponent} from './view-simulacoes/view-simulacoes.component';

export const routes: Routes = [
  { path: '', redirectTo: '/cadastrar-simulacao', pathMatch: 'full' },
  { path: 'home', component: AppComponent },
  { path: 'cadastrar-simulacao', component: SimulacaoComponent },
  { path: 'simulacoes', component: ViewSimulacoesComponent },
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
