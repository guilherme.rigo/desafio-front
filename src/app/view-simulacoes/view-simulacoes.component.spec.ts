import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSimulacoesComponent } from './view-simulacoes.component';

describe('ViewSimulacoesComponent', () => {
  let component: ViewSimulacoesComponent;
  let fixture: ComponentFixture<ViewSimulacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSimulacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSimulacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
