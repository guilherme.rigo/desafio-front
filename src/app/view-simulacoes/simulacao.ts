import DateTimeFormat = Intl.DateTimeFormat;

export class Simulacao {
  nomePessoa: string;
  cpf: string;
  valorSegurado: string;
  numeroContratoEmprestimo: string;
  fimContratoEmprestimo: string;
  dataNascimento: string;
  produtoEscolhido: {
    nome: string;
    idadeMinima: number;
    idadeMaxima: number;
    taxaJuros: string;
    valorMinimoPremio: string
  };
  valorTotalPremio: string;
  dataSimulacao: string;
}
