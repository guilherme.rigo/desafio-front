import { Component, OnInit } from '@angular/core';
import {RequestService} from '../request.service';
import {Simulacao} from './simulacao';
import * as moment from 'moment';
@Component({
  selector: 'app-view-simulacoes',
  templateUrl: './view-simulacoes.component.html',
  styleUrls: ['./view-simulacoes.component.css']
})
export class ViewSimulacoesComponent implements OnInit {

  constructor(private requestService: RequestService) { }
  public simulacoes = new Array<Simulacao>();
  public dataS;
  public dataF;
  getSimulacoes(): void {
    this.requestService.get('simulacoes').subscribe(res => {
      this.simulacoes = res.data;
      console.log(this.simulacoes);
      this.simulacoes.forEach(d => {
        d.fimContratoEmprestimo = moment(d.fimContratoEmprestimo).format('DD/MM/YYYY');
        d.dataSimulacao = moment(d.dataSimulacao).format('DD/MM/YYYY');
      });
    });

  }
  ngOnInit(): void {
    this.getSimulacoes();
  }

}
