import { Component, OnInit } from '@angular/core';
import {RequestService} from '../request.service';

@Component({
  selector: 'app-simulacao',
  templateUrl: './simulacao.component.html',
  styleUrls: ['./simulacao.component.css']
})
export class SimulacaoComponent implements OnInit {

  constructor(private requestsService: RequestService) { }
  public teste: boolean;
  public error: string;
  public valorPremio: string;
  public produtoEscolhido: string;
  public href: string;
  public simulacao = {
    nomePessoa: null,
    cpf: null,
    valorSegurado: null,
    numeroContratoEmprestimo: null,
    fimContratoEmprestimo: null,
    dataNascimento: null
  };
  // tslint:disable-next-line:typedef
   submitSimulacao(data) {
    console.log(data);
    const simulacao = {
      nomePessoa: data.nomePessoa,
      cpf: data.cpf,
      valorSegurado: data.valorSegurado,
      numeroContratoEmprestimo: data.numeroContratoEmprestimo,
      fimContratoEmprestimo: data.fimContratoEmprestimo,
      dataNascimento: data.dataNascimento
    };
    this.requestsService.post('cadastrar-simulacao', simulacao).subscribe(res => {
        this.teste = true;
        console.log(res)
        this.produtoEscolhido = res.data.produtoEscolhido.nome;
        this.valorPremio = 'R$' + res.data.valorTotalPremio;
        this.href = '#simulacaoOK';
    },
      error1 => {
        this.error = error1.error.errors[0];
        this.teste = false;
        this.href = '#simulacaoERROR';

      }
    );
  }
  hrefType(teste) {
    if(teste==true)
      this.href = '#simulacaoERROR';
    if(teste==false)
      this.href = '#simulacaoOK';
  }
  setProdutos(): void{
     const data = null;
     this.requestsService.get('set-produto').subscribe(res => {}, error1 => { this.error = 'Servidor Offline.'; this.teste = false });
  }
  ngOnInit(): void {
     this.setProdutos();
  }

}
