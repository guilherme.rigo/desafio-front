import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestService {

  constructor(private http: HttpClient) { }
  protected headers = { headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })};

 private body = {
      "nomePessoa": "Guilherme",
      "cpf": 10828071926,
      "valorSegurado": 2000,
      "numeroContratoEmprestimo": 1234,
      "fimContratoEmprestimo": "2030-03-19",
      "dataNascimento": "2000-07-17"
  };
  get(URL: string, params = null): Observable<any> {
    if(params) {
      return this.http.get<any>('http://localhost:8080/' + URL + params.id);
    }
    return this.http.get<any>('http://localhost:8080/' + URL);
  }
  post(URL: string, data: any): Observable<any> {
    const dataS = JSON.parse(JSON.stringify(data));
    return this.http.post<any>('http://localhost:8080/' + URL, dataS, this.headers);
  }
}
