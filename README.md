# DesafioFront

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.7.

## Setup

```bash
$ sudo apt install node
$ sudo npm i -g --update npm
$ sudo npm i -g node-sass @angular/cli
$ npm install
$ npm start
```
//Após a inicializacao do servidor acessar a URL=http://localhost:4200
## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
